package geometry;

public class Triangle {
    private double width;
    private double height;

    public Triangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return this.width;
    }
    public double getHeight() {
        return this.height;
    }

    public double getArea() {
        return (this.width * this.height) / 2;
    }

    public String toString() {
        return ("Height" + this.height + "\nWidth" + this.width);
    }
}
