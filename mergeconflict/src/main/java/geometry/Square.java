package geometry;

public class Square {
    private double side;
    public Square(double side){
        this.side = side;
    }

    public double getSide(){
        return this.side;
    }

    public double getArea(){
        return side*side;
    }

    public String toString(){
        return "this is a square side: " + side;
    }
}
