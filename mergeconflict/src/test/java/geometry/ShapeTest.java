package geometry;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class ShapeTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test

    public void testSquareArea(){
        Square square = new Square(4);
        assertEquals(square.getArea(), 16, 0);
    }

    @Test
    public void testSquareSide(){
        Square square = new Square(4);
        assertEquals(square.getSide(), 4, 0);
    }

    @Test
    public void testSquare(){
        Square square = new Square(4);
        String str ="this is a square side: " + 4.0;
        assertEquals(str, square.toString());
    }
    
    @Test
    public void triangle_getWidth_return2()
    {
        Triangle tri = new Triangle(2, 6);
        assertEquals("This should return 2", 2, tri.getWidth(), 0);
    }

    @Test
    public void triangle_getWidth_return6()
    {
        Triangle tri = new Triangle(2, 6);
        assertEquals("This should return 6", 6, tri.getHeight(), 0);
    }

    @Test
    public void triangle_getArea_return6()
    {
        Triangle tri = new Triangle(2, 6);
        assertEquals("This should return 6", 6, tri.getArea(), 0);
    }

    @Test
    public void triangle_getWidth_return()
    {
        Triangle tri = new Triangle(2, 6);
        String str = "Height" + 6.0 + "\nWidth" + 2.0;
        assertEquals("This should return 6", str, tri.toString());
    }


}
